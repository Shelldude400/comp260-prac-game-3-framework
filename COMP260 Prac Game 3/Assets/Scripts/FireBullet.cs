﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {
	public BulletMove bulletPrefab;
	public float reloadTimer = 0.5f;
	private float tempTimer;
	// Use this for initialization
	void Start () {
		tempTimer = reloadTimer;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeScale == 0) {
			return;
		}
		// when the button is pushed, fire a bullet
		if (Input.GetButtonDown("Fire1") && tempTimer < 0) {

			BulletMove bullet = Instantiate(bulletPrefab);
			// the bullet starts at the player's position
			bullet.transform.position = transform.position;

			// create a ray towards the mouse location
			Ray ray = 
				Camera.main.ScreenPointToRay(Input.mousePosition);
			bullet.direction = ray.direction;
			tempTimer = reloadTimer;
		}

		tempTimer -= Time.deltaTime;

	}

}
