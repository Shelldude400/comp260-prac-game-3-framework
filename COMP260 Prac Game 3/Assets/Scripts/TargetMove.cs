﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour {

	private Animator animator;
	public float startTime = 0.0f;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
		// set the Start parameter to true 
		// if we have passed the start time
		animator.SetBool("Start", Time.time >= startTime);
	}

	void Destroy() {
		Destroy(gameObject);
	}

	void OnCollisionEnter(Collision collision) {
		// transition to the Die animation
		animator.SetBool("Hit", true);
	}
}
